package com.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.order.domain.Item;
import com.order.domain.Restaurant;
import com.order.dto.request.ItemDto;
import com.order.dto.response.ItemResponseDto;
import com.order.exception.BadRequestException;
import com.order.exception.ForbiddenException;
import com.order.repository.ItemRepository;
import com.order.repository.RestaurantRepository;

@Service
public class ItemService {
	private ItemRepository itemRepository;
	private RestaurantRepository restaurantRepository;
	Item item = new Item();
	ItemResponseDto itemResponseDto = new ItemResponseDto();
	
	@Autowired
	public ItemService(ItemRepository itemRepository, RestaurantRepository restaurantRepository) {
		super();
		this.restaurantRepository = restaurantRepository;
		this.itemRepository = itemRepository;
		
	}
	
	public Item createItem(ItemDto itemDto) throws ForbiddenException{
		item.setItemId(itemDto.getItemId());
		item.setName(itemDto.getName());
		item.setPrice(itemDto.getPrice());
		item.setStock(itemDto.getStock());
		Restaurant restaurant = new Restaurant();
		restaurant = restaurantRepository.findRestaurantByRestaurantId(itemDto.getRestaurantId());
		item.setRestaurant(restaurant);
		return itemRepository.save(item);
	} 
	
	public ItemResponseDto retrieveItem(long id) throws BadRequestException {
		if(itemRepository.findItemByItemId(id) == null) {
			throw new BadRequestException("ITEM WITH ID : "+id+" DOES NOT EXIST");
		}
		item = itemRepository.findById(id).orElse(null);
		itemResponseDto.setItemId(item.getItemId());
		itemResponseDto.setRestaurantName(item.getRestaurant().getName());
		itemResponseDto.setItemName(item.getName());
		itemResponseDto.setPrice(item.getPrice());
		itemResponseDto.setStock(item.getStock());
		return itemResponseDto;
	}
	
	public List<Item> retireveAllItems(){
		return itemRepository.findAll();
	}
	
	public String updateItem(ItemDto itemDto) throws ForbiddenException {
		createItem(itemDto);
		return "Item with id : "+itemDto.getItemId()+"has been updated";
	}
	
	public String deleteItem(long id) throws BadRequestException {
		if(itemRepository.findItemByItemId(id) == null) {
			throw new BadRequestException("ITEM WITH ID : "+id+" DOES NOT EXIST");
		}
		itemRepository.deleteById(id);
		return "Item with id : "+id+"has been deleted";
	}
}

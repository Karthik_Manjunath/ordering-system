package com.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.order.domain.Restaurant;
import com.order.dto.request.RestaurantDto;
import com.order.exception.BadRequestException;
import com.order.repository.RestaurantRepository;

@Service
public class RestaurantService {
	private RestaurantRepository restaurantRepository; 
	Restaurant restaurant = new Restaurant();
	
	public RestaurantService() {
		
	}

	@Autowired
	public RestaurantService(RestaurantRepository restaurantRepository) {
		super();
		this.restaurantRepository = restaurantRepository;
	}
	
	public Restaurant addRestaurant(RestaurantDto restaurantDto) {
		
		restaurant.setRestaurantId(restaurantDto.getRestaurantId());
		restaurant.setName(restaurantDto.getName());
		restaurant.setPhoneNumber(restaurantDto.getPhoneNumber());
		restaurant.setAddress(restaurantDto.getAddress());
		restaurant.setDiscount(restaurantDto.getDiscount());
		restaurant.setRating(restaurantDto.getRating());
		return restaurantRepository.save(restaurant);
	}
	
	public Restaurant getRestaurant(long id) throws BadRequestException{
		if(restaurantRepository.findRestaurantByRestaurantId(id) == null) {
			throw new BadRequestException("RESTAURANT WITH ID : "+id+" DOES NOT EXIST");
		}
		return restaurantRepository.findById(id).orElse(null);
	}
	
	public List<Restaurant> getAllRestaurants(){
		return restaurantRepository.findAll();
	}
	
	public void updateRestaurant(RestaurantDto restaurantDto) {
		addRestaurant(restaurantDto);
	}
	
	public void deleteRestaurant(long id) throws BadRequestException {
		if(restaurantRepository.findRestaurantByRestaurantId(id) == null) {
			throw new BadRequestException("RESTAURANT WITH ID : "+id+" DOES NOT EXIST");
		}
		restaurantRepository.deleteById(id);
	}
	
	public void deleteAllRestaurants() {
		restaurantRepository.deleteAll();
	}
}

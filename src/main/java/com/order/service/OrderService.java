package com.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.order.domain.Item;
import com.order.domain.ItemsOrdered;
import com.order.domain.Order;
import com.order.domain.Restaurant;
import com.order.domain.User;
import com.order.dto.request.OrderDto;
import com.order.dto.response.OrderResponseDto;
import com.order.exception.BadRequestException;
import com.order.repository.OrderRepository;
import com.order.repository.RestaurantRepository;
import com.order.repository.UserRepository;

@Service
public class OrderService {
	
	private OrderRepository orderRepository;
	private UserRepository userRepository;
	private RestaurantRepository restaurantRepository;
	private OrderResponseDto orderResponseDto = new OrderResponseDto();
	Order order = new Order();
	Restaurant restaurant = new Restaurant();
	public OrderService() {
		
	}

	@Autowired
	public OrderService(OrderRepository orderRepository, UserRepository userRepository, RestaurantRepository restaurantRepository) {
		super();
		this.orderRepository = orderRepository;
		this.userRepository = userRepository;
		this.restaurantRepository = restaurantRepository;
	}
	
	public int calculateTotal(Order order) {
		int total = 0;
		List<ItemsOrdered> itemsOrderedList = order.getItemsOrderedList();
		for(ItemsOrdered item : itemsOrderedList) {
			Item neededItem = item.getItem();
			total += item.getQuantity() * neededItem.getPrice();
		}
		return total;
	}
	
	public Order addOrder(OrderDto orderDto) throws BadRequestException {
		order.setOrderId(orderDto.getOrderId());
		long custId = orderDto.getCustId();
		long restaurantId = orderDto.getRestaurantId();
		User user = new User();
		user = userRepository.findUserByUid(custId);
		Restaurant restaurant = new Restaurant();
		restaurant = restaurantRepository.findRestaurantByRestaurantId(restaurantId);
		order.setUser(user);
		order.setRestaurant(restaurant);
		return orderRepository.save(order);
	}
	
	public Order getOrder(long id) throws BadRequestException{
		if(orderRepository.findOrderByOrderId(id) == null) {
			throw new BadRequestException("ORDER WITH ID : "+id+" DOES NOT EXIST");
		}
		return orderRepository.findById(id).orElse(null);
	}
	
	public OrderResponseDto getBillingDetails(long orderId) {
		order = orderRepository.findOrderByOrderId(orderId);
		orderResponseDto.setUserId(order.getUser().getUid());
		orderResponseDto.setRestaurantId(order.getRestaurant().getRestaurantId());
		orderResponseDto.setTax(5);
		int total = calculateTotal(order);
		orderResponseDto.setTotal(total);
		order.setTotal(total);
		restaurant = orderRepository.findOrderByOrderId(orderId).getRestaurant();
		double finalAmount = (1.05 * total) - ((restaurant.getDiscount()/100) * total);
		order.setFinalAmount(finalAmount);
		order.setTax(5);
		orderRepository.save(order);
		orderResponseDto.setFinalAmount(finalAmount);
		return orderResponseDto;
	}
	
	public List<Order> getAllOrders(){
		return orderRepository.findAll();
	}
	
	public List<ItemsOrdered> getItemsOrderedList(long id){
		order = orderRepository.findOrderByOrderId(id);
		return order.getItemsOrderedList();
	}
	
	
	
	public void updateOrder(OrderDto orderDto) throws BadRequestException {
		addOrder(orderDto);
	}
	
	public void deleteOrder(long id) throws BadRequestException {
		if(orderRepository.findOrderByOrderId(id) == null) {
			throw new BadRequestException("ORDER WITH ID : "+id+" DOES NOT EXIST");
		}
		orderRepository.deleteById(id);
	}
	
	public void deleteAllOrders() {
		orderRepository.deleteAll();
	}
}

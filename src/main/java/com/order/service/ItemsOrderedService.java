package com.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.order.domain.Item;
import com.order.domain.ItemsOrdered;
import com.order.domain.Order;
import com.order.dto.request.ItemsOrderedDto;
import com.order.dto.response.ItemsOrderedResponseDto;
import com.order.exception.BadRequestException;
import com.order.exception.ForbiddenException;
import com.order.repository.ItemRepository;
import com.order.repository.ItemsOrderedRepository;
import com.order.repository.OrderRepository;

@Service
public class ItemsOrderedService {
	private ItemsOrderedRepository itemsOrderedRepository;
	
	private ItemRepository itemRepository;
	private OrderRepository orderRepository;
	
	ItemsOrdered itemsOrdered = new ItemsOrdered();
	ItemsOrderedResponseDto itemsOrderedResponseDto = new ItemsOrderedResponseDto();
	
	@Autowired
	public ItemsOrderedService(ItemsOrderedRepository itemsOrderedRepository, ItemRepository itemRepository, OrderRepository orderRepository) {
		super();
		this.itemsOrderedRepository = itemsOrderedRepository;
		this.itemRepository = itemRepository;
		this.orderRepository = orderRepository;
	}
	
	
	public void updateStock(ItemsOrderedDto itemsOrderedDto) throws BadRequestException {
		Item neededItem = itemRepository.findItemByItemId(itemsOrderedDto.getItemId());
		int currentStock = neededItem.getStock() - itemsOrderedDto.getQuantity();
		if(currentStock < 0) {
			throw new BadRequestException("The item, "+ neededItem.getName()+ "is unavailable. Lower the quantity and try again");
		}
		neededItem.setStock(currentStock);
	}
	
	public ItemsOrdered createItemsOrderedList(ItemsOrderedDto itemsOrderedDto) throws BadRequestException{
		itemsOrdered.setItemsOrderedId(itemsOrderedDto.getItemsOrderedId());
		Item item = new Item();
		item = itemRepository.findItemByItemId(itemsOrderedDto.getItemId());
		Order order = new Order();
		order = orderRepository.findOrderByOrderId(itemsOrderedDto.getOrderId());
		itemsOrdered.setItem(item);
		itemsOrdered.setOrder(order);
		itemsOrdered.setQuantity(itemsOrderedDto.getQuantity());
		updateStock(itemsOrderedDto);
		return itemsOrderedRepository.save(itemsOrdered);
	} 
	
	public ItemsOrderedResponseDto retrieveItemsOrderedList(long id) throws BadRequestException {
		if(itemsOrderedRepository.findItemsOrderedByItemsOrderedId(id) == null) {
			throw new BadRequestException("ITEMS ORDERED LIST WITH ID : "+id+" DOES NOT EXIST");
		}
		itemsOrdered = itemsOrderedRepository.findById(id).orElse(null);
		itemsOrderedResponseDto.setItemsOrderedid(itemsOrdered.getItemsOrderedId());
		itemsOrderedResponseDto.setItemName(itemsOrdered.getItem().getName());
		itemsOrderedResponseDto.setOrderId(itemsOrdered.getOrder().getOrderId());
		itemsOrderedResponseDto.setQuantity(itemsOrdered.getQuantity());
		return itemsOrderedResponseDto;
	}
	
	public List<ItemsOrdered> retireveAllItemsOrderedLists(){
		return itemsOrderedRepository.findAll();
	}
	
	public String updateItemsOrderedList(ItemsOrderedDto itemsOrderedDto) throws ForbiddenException, BadRequestException {
		createItemsOrderedList(itemsOrderedDto);
		return "Item ordered with id : "+itemsOrderedDto.getItemId()+"has been updated";
	}
	
	public String deleteItemsOrderedList(long id) throws BadRequestException {
		if(itemsOrderedRepository.findById(id) == null) {
			throw new BadRequestException("ITEMS ORDERED LIST WITH ID : "+id+" DOES NOT EXIST");
		}
		itemsOrderedRepository.deleteById(id);
		return "Item ordered with id : "+id+"has been deleted";
	}
		
}

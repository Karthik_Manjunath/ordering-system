package com.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.order.domain.Order;
import com.order.domain.User;
import com.order.dto.request.UserDto;
import com.order.exception.BadRequestException;
import com.order.exception.ForbiddenException;
import com.order.repository.UserRepository;

@Service
public class UserService {
	
	private UserRepository userRepository;
	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	User user = new User();
	@Autowired
	public UserService(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}
	
	public User createUser(UserDto userDto) throws ForbiddenException{
		if(userDto.getPhno().length() > 10) {
			throw new ForbiddenException("User's Phone number can have a maximum of ten digits");
		}
		user.setUid(userDto.getId());
		user.setName(userDto.getName());
		user.setPhoneNumber(userDto.getPhno());
		user.setEmailId(userDto.getEmailId());
		user.setAddress(userDto.getAddress());
		user.setPassword(encoder.encode(userDto.getPassword()));
		return userRepository.save(user);
	} 
	
	public User retrieveUser(long id) throws BadRequestException {
		if(userRepository.findUserByUid(id) == null) {
			throw new BadRequestException("USER WITH ID : "+id+" DOES NOT EXIST");
		}
		return userRepository.findById(id).orElse(null);
	}
	
	public List<User> retireveAllUsers(){
		return userRepository.findAll();
	}
	
	public void updateUser(UserDto userDto) throws ForbiddenException {
		createUser(userDto);
	}
	
	public void deleteUser(long id) throws BadRequestException {
		if(userRepository.findUserByUid(id) == null) {
			throw new BadRequestException("USER WITH ID : "+id+" DOES NOT EXIST");
		}
		userRepository.deleteById(id);
	}
	
	public List<Order> listAllOrders(long id) throws BadRequestException{
		if(userRepository.findUserByUid(id) == null) {
			throw new BadRequestException("USER WITH ID : "+id+" DOES NOT EXIST");
		}
		user = userRepository.findUserByUid(id);
		
		return user.getOrders();
	}
	
}

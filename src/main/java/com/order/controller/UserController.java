package com.order.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.order.domain.Order;
import com.order.domain.User;
import com.order.dto.request.UserDto;
import com.order.exception.BadRequestException;
import com.order.exception.ForbiddenException;
import com.order.service.UserService;

import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/user")
public class UserController {
	private UserService userService;
	private static final String STR = "User with id: ";
	@Autowired
	UserController(UserService userService) {
		super();
		this.userService = userService;
	}
	
	@ApiOperation(value = "To create a user")
	@PostMapping(value = "/create")
	public User createUser(@RequestBody UserDto userDto) throws ForbiddenException {
		return userService.createUser(userDto);
	}
	
	@ApiOperation(value = "To display a user by his/her id")
	@GetMapping(value = "/show/{id}")
    public @ResponseBody User retrieveUser(@PathVariable(name = "id") long id) throws BadRequestException {
        return userService.retrieveUser(id);
    }

	@ApiOperation(value = "To display all the users")
	@GetMapping(value = "/show")
	public @ResponseBody List<User> retriveAllUsers(){
		return userService.retireveAllUsers();
	}
	
	@GetMapping(value = "/listAllOrders/{id}")
	public @ResponseBody List<Order> listAllOrders(@PathVariable(name = "id") long id) throws BadRequestException{
		return userService.listAllOrders(id);
 	}
	
	@ApiOperation(value = "To update a user's details")
	@PutMapping(value = "/update")
	public String updateUser(@RequestBody UserDto userDto) throws ForbiddenException  {
		userService.updateUser(userDto);
		return STR+userDto.getId()+" has been updated";
	}
	
	@ApiOperation(value = "To delete a user by id")
	@DeleteMapping(value = "/delete/{id}")
	public String deleteUser(@PathVariable(name = "id") long id) throws BadRequestException {
		userService.deleteUser(id);
		return STR+id+" has been deleted.";
	}
}

package com.order.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.order.domain.Restaurant;
import com.order.dto.request.RestaurantDto;
import com.order.exception.BadRequestException;
import com.order.exception.ForbiddenException;
import com.order.service.RestaurantService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/restaurant")
public class RestaurantController {
	private RestaurantService restaurantService;
	private static final String STR = "Restaurant with id: ";
	
	@Autowired
	RestaurantController(RestaurantService restaurantService) {
		super();
		this.restaurantService = restaurantService;
	}
	
	@ApiOperation(value = "To create a restaurant")
	@PostMapping(value = "/create")
	public Restaurant createRestaurant(@RequestBody RestaurantDto restaurantDto) throws ForbiddenException {
		return restaurantService.addRestaurant(restaurantDto);
	}
	
	@ApiOperation(value = "To display a restaurant by its id")
	@GetMapping(value = "/show/{id}")
    public @ResponseBody Restaurant retrieveRestaurant(@PathVariable(name = "id") long id) throws BadRequestException {
        return restaurantService.getRestaurant(id);
    }

	@ApiOperation(value = "To display all the restaurants")
	@GetMapping(value = "/show")
	public @ResponseBody List<Restaurant> retriveAllRestaurants(){
		return restaurantService.getAllRestaurants();
	}
	
	@ApiOperation(value = "To update a restaurant's details")
	@PutMapping(value = "/update")
	public String updateRestaurant(@RequestBody RestaurantDto restaurantDto) throws ForbiddenException  {
		restaurantService.updateRestaurant(restaurantDto);
		return STR+restaurantDto.getRestaurantId()+" has been updated";
	}
	
	@ApiOperation(value = "To delete a restaurant by id")
	@DeleteMapping(value = "/delete/{id}")
	public String deleteRestaurant(@PathVariable(name = "id") long id) throws BadRequestException {
		restaurantService.deleteRestaurant(id);
		return STR+id+" has been deleted.";
	}
}

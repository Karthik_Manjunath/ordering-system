package com.order.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.order.domain.ItemsOrdered;
import com.order.dto.request.ItemsOrderedDto;
import com.order.dto.response.ItemsOrderedResponseDto;
import com.order.exception.BadRequestException;
import com.order.exception.ForbiddenException;
import com.order.service.ItemsOrderedService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("ItemsOrderedList")
public class ItemsOrderedController {
	private ItemsOrderedService itemsOrderedService;
	private static final String STR = "Items Ordered List with id: ";
	
	@Autowired
	ItemsOrderedController(ItemsOrderedService itemsOrderedService) {
		super();
		this.itemsOrderedService = itemsOrderedService;
	}
	
	@ApiOperation(value = "To create an Items Ordered List")
	@PostMapping(value = "/create")
	public ItemsOrdered createItemsOrderedList(@RequestBody ItemsOrderedDto itemsOrderedDto) throws ForbiddenException, BadRequestException {
		return itemsOrderedService.createItemsOrderedList(itemsOrderedDto);
	}
	
	@ApiOperation(value = "To display an items ordered list by its id")
	@GetMapping(value = "/show/{id}")
    public @ResponseBody ItemsOrderedResponseDto retrieveItemsOrderedList(@PathVariable(name = "id") long id) throws BadRequestException {
        return itemsOrderedService.retrieveItemsOrderedList(id);
    }

	@ApiOperation(value = "To display all the lists of items ordered")
	@GetMapping(value = "/show")
	public @ResponseBody List<ItemsOrdered> retriveAllItemsOrderedLists(){
		return itemsOrderedService.retireveAllItemsOrderedLists();
	}
	
	@ApiOperation(value = "To update an items ordered list's details")
	@PutMapping(value = "/update")
	public String updateItemdOrderedList(@RequestBody ItemsOrderedDto itemsOrderedDto) throws ForbiddenException, BadRequestException  {
		itemsOrderedService.updateItemsOrderedList(itemsOrderedDto);
		return STR+itemsOrderedDto.getItemsOrderedId()+" has been updated";
	}
	
	@ApiOperation(value = "To delete a list of items ordered by id")
	@DeleteMapping(value = "/delete/{id}")
	public String deleteItemsOrdered(@PathVariable(name = "id") long id) throws BadRequestException {
		itemsOrderedService.deleteItemsOrderedList(id);
		return STR+id+" has been deleted.";
	}
}

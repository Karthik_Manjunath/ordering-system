package com.order.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.order.domain.ItemsOrdered;
import com.order.domain.Order;
import com.order.dto.request.OrderDto;
import com.order.dto.response.OrderResponseDto;
import com.order.exception.BadRequestException;
import com.order.service.OrderService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/order")
public class OrderController {
	
	private OrderService orderService;
	private static final String STR = "Order with Id: ";
	@Autowired
	public OrderController(OrderService orderService) {
		super();
		this.orderService = orderService;
	}
	
	@ApiOperation(value = "To create a new order")
	@PostMapping(value = "/create")
	public Order addOrder(@RequestBody OrderDto orderDto) throws BadRequestException {
		 return orderService.addOrder(orderDto);
	}
	
	@ApiOperation(value = "To display an order by its id")
	@GetMapping(value = "/show/{id}")
    public @ResponseBody Order getOrder(@PathVariable(name = "id") long id)  throws BadRequestException{
		if(orderService.getOrder(id) == null) {
			throw new BadRequestException("ORDER WITH ID : "+id+" DOES NOT EXIST");
		}
        return orderService.getOrder(id);
    }

	@ApiOperation(value = "To display all the orders")
	@GetMapping(value = "/show")
	public @ResponseBody List<Order> getAllOrders(){
		return orderService.getAllOrders();
	}
	
	@ApiOperation(value = "To display the list of ordered items by order id")
	@GetMapping(value = "/showOrderedItems/{id}")
	public @ResponseBody List<ItemsOrdered> getItemsOrderedListById(@PathVariable(name = "id") long id){
		return orderService.getItemsOrderedList(id);
	}
	
	@ApiOperation(value = "To update an order")
	@PutMapping(value = "/update")
	public String updateOrder(@RequestBody OrderDto orderDto) throws BadRequestException {
		orderService.updateOrder(orderDto);
		return STR+orderDto.getOrderId()+" has been updated.";
	}
	
	@ApiOperation(value = "To get billing details by the order id")
	@GetMapping(value = "/bill/{id}")
	public OrderResponseDto getBillingDetails(@PathVariable(name = "id") long id) throws BadRequestException{
		return orderService.getBillingDetails(id);
		
	}
	
	@ApiOperation(value = "To delete an order by its id")
	@DeleteMapping(value = "/delete/{id}")
	public String deleteOrder(@PathVariable(name = "id") long id) throws BadRequestException{
		orderService.deleteOrder(id);
		return STR+id+" has been deleted.";
	}
	
	@ApiOperation(value = "To delete all the orders")
	@DeleteMapping(value = "/delete")
	public String deleteAllOrders() {
		orderService.deleteAllOrders();
		return "All orders have been deleted";
	}
	
}

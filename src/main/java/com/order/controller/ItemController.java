package com.order.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.order.domain.Item;
import com.order.dto.request.ItemDto;
import com.order.dto.response.ItemResponseDto;
import com.order.exception.BadRequestException;
import com.order.exception.ForbiddenException;
import com.order.service.ItemService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/item")
public class ItemController {
	private ItemService itemService;
	private static final String STR = "Item with id: ";
	@Autowired
	ItemController(ItemService itemService) {
		super();
		this.itemService = itemService;
	}
	
	@ApiOperation(value = "To create an item")
	@PostMapping(value = "/create")
	public Item createItem(@RequestBody ItemDto itemDto) throws ForbiddenException {
		return itemService.createItem(itemDto);
	}
	
	@ApiOperation(value = "To display an item by its id")
	@GetMapping(value = "/show/{id}")
    public @ResponseBody ItemResponseDto retrieveItem(@PathVariable(name = "id") long id) throws BadRequestException {
        return itemService.retrieveItem(id);
    }

	@ApiOperation(value = "To display all the items")
	@GetMapping(value = "/show")
	public @ResponseBody List<Item> retriveAllItems(){
		return itemService.retireveAllItems();
	}
	
	@ApiOperation(value = "To update an item's details")
	@PutMapping(value = "/update")
	public String updateItem(@RequestBody ItemDto itemDto) throws ForbiddenException  {
		itemService.updateItem(itemDto);
		return STR+itemDto.getItemId()+" has been updated";
	}
	
	@ApiOperation(value = "To delete a item by id")
	@DeleteMapping(value = "/delete/{id}")
	public String deleteItem(@PathVariable(name = "id") long id) throws BadRequestException {
		itemService.deleteItem(id);
		return STR+id+" has been deleted.";
	}
}

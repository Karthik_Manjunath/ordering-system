package com.order.dto.response;

public class ItemsOrderedResponseDto {
	private long itemsOrderedid;
	private String itemName;
	private long orderId;
	private int quantity;
	
	public ItemsOrderedResponseDto() {
		
	}
	
	private ItemsOrderedResponseDto(long itemsOrderedid, String itemName, long orderId, int quantity) {
		super();
		this.itemsOrderedid = itemsOrderedid;
		this.itemName = itemName;
		this.orderId = orderId;
		this.quantity = quantity;
	}
	
	public long getItemsOrderedid() {
		return itemsOrderedid;
	}
	public void setItemsOrderedid(long itemsOrderedid) {
		this.itemsOrderedid = itemsOrderedid;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}

package com.order.dto.response;

public class OrderResponseDto {
	private long restaurantId;
	private long userId;
	private int total;
	private int tax;
	private double finalAmount;
	
	
	
	private OrderResponseDto(long restaurantId, long userId, int total, int tax, double finalAmount) {
		super();
		this.restaurantId = restaurantId;
		this.userId = userId;
		this.total = total;
		this.tax = tax;
		this.finalAmount = finalAmount;
	}
	
	public OrderResponseDto() {
		
	}
	
	public long getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(long restaurantId) {
		this.restaurantId = restaurantId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getTax() {
		return tax;
	}
	public void setTax(int tax) {
		this.tax = tax;
	}
	public double getFinalAmount() {
		return finalAmount;
	}
	public void setFinalAmount(double finalAmount) {
		this.finalAmount = finalAmount;
	}
	
	
}

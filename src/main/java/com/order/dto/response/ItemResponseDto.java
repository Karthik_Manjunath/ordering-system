package com.order.dto.response;

public class ItemResponseDto {
	private long itemId;
	private String restaurantName;
	private String itemName;
	private int price;
	private int stock;
	
	public ItemResponseDto() {
		
	}
	
	private ItemResponseDto(long itemId, String restaurantName, String itemName, int price, int stock) {
		super();
		this.itemId = itemId;
		this.restaurantName = restaurantName;
		this.itemName = itemName;
		this.price = price;
		this.stock = stock;
	}
	
	public long getItemId() {
		return itemId;
	}
	public void setItemId(long itemId) {
		this.itemId = itemId;
	}
	public String getRestaurantName() {
		return restaurantName;
	}
	public void setRestaurantName(String restaurantName) {
		this.restaurantName = restaurantName;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	
	
}

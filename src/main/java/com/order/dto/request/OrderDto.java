package com.order.dto.request;

public class OrderDto {
	private long orderId;
	private long custId;
	private long restaurantId;

	
	private OrderDto(long orderId, long custId, long restaurantId) {
		super();
		this.orderId = orderId;
		this.custId = custId;
		this.restaurantId = restaurantId;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getCustId() {
		return custId;
	}

	public void setCustId(long custId) {
		this.custId = custId;
	}

	public long getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(long restaurantId) {
		this.restaurantId = restaurantId;
	}

}

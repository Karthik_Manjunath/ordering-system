package com.order.dto.request;

public class UserDto {
	private long id;
	private String name;
	private String phno;
	private String emailId;
	private String password;
	private String address;
	
	private UserDto(long id, String name, String phno, String emailId, String password, String address) {
		super();
		this.id = id;
		this.name = name;
		this.phno = phno;
		this.emailId = emailId;
		this.password = password;
		this.address = address;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhno() {
		return phno;
	}

	public void setPhno(String phno) {
		this.phno = phno;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}

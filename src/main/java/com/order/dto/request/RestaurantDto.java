package com.order.dto.request;

public class RestaurantDto {
	
	private long restaurantId;
	
	private String name;
	
	private String phoneNumber;
	
	private String address;
	
	private int discount;
	
	private int rating;

	private RestaurantDto(long restaurantId, String name, String phoneNumber, String address, int discount,
			int rating) {
		super();
		this.restaurantId = restaurantId;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.discount = discount;
		this.rating = rating;
	}

	public long getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(long restaurantId) {
		this.restaurantId = restaurantId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	
	
}

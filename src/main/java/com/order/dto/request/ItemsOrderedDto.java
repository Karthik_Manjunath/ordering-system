package com.order.dto.request;

public class ItemsOrderedDto {
	
	private long itemsOrderedId;
	private long itemId;
	private long orderId;
	private int quantity;
	
	private ItemsOrderedDto(long itemsOrderedId, long itemId, long orderId, int quantity) {
		super();
		this.itemsOrderedId = itemsOrderedId;
		this.itemId = itemId;
		this.orderId = orderId;
		this.quantity = quantity;
	}
	public long getItemsOrderedId() {
		return itemsOrderedId;
	}
	public void setItemsOrderedId(long itemsOrderedId) {
		this.itemsOrderedId = itemsOrderedId;
	}
	public long getItemId() {
		return itemId;
	}
	public void setItemId(long itemId) {
		this.itemId = itemId;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}

package com.order.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "items_ordered")
public class ItemsOrdered {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "items_ordered_id")
	private long itemsOrderedId;
	
	@OneToOne
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@JoinColumn(name = "item_id", updatable = false)
	private Item item;
	
	
	@ManyToOne
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@JoinColumn(name = "order_id", updatable = false)
	private Order order;
	
	int quantity;

	private ItemsOrdered(long itemsOrderedId, Item item, Order order, int quantity) {
		super();
		this.itemsOrderedId = itemsOrderedId;
		this.item = item;
		this.order = order;
		this.quantity = quantity;
	}

	public ItemsOrdered() {
		
	}

	public long getItemsOrderedId() {
		return itemsOrderedId;
	}

	public void setItemsOrderedId(long itemsOrderedId) {
		this.itemsOrderedId = itemsOrderedId;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}

package com.order.domain;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
@Table(name = "orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long orderId;
	
	@ManyToOne
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@JoinColumn(name = "restaurant_id", updatable = false)
	Restaurant restaurant;
	
	@ManyToOne
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@JoinColumn(name = "cust_id", updatable = false)
	private User user;
	
	private int total;
	
	private int tax;
	
	@Column(name = "final_amount")
	private double finalAmount;
	
	@OneToMany(mappedBy = "order")
	private List<ItemsOrdered> ItemsOrderedList;
	
	public Order() {
	}


	private Order(long orderId, Restaurant restaurant, User user, int total, int tax, int finalAmount) {
		super();
		this.orderId = orderId;
		this.restaurant = restaurant;
		this.user = user;
		this.total = total;
		this.tax = tax;
		this.finalAmount = finalAmount;
	}


	public long getOrderId() {
		return orderId;
	}


	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}


	public Restaurant getRestaurant() {
		return restaurant;
	}


	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public int getTotal() {
		return total;
	}


	public void setTotal(int total) {
		this.total = total;
	}


	public int getTax() {
		return tax;
	}


	public void setTax(int tax) {
		this.tax = tax;
	}


	public double getFinalAmount() {
		return finalAmount;
	}


	public void setFinalAmount(double finalAmount) {
		this.finalAmount = finalAmount;
	}


	public List<ItemsOrdered> getItemsOrderedList() {
		return ItemsOrderedList;
	}


	public void setItemsOrderedList(List<ItemsOrdered> itemsOrderedList) {
		ItemsOrderedList = itemsOrderedList;
	}

	
	
}

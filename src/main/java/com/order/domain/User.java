package com.order.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "user")
public class User implements Serializable{
	
	private static final long serialVersionUID = 2962524015752766690L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private long uid;
	
	private String name;
	
	@Column(name = "phno")
	private String phoneNumber;
	
	@Column(name = "email_id")
	private String emailId;
		
	private String password;
	
	private String address;
	
	@OneToMany(mappedBy = "user")
	private List<Order> orders;

	public User() {
	}
	
	private User(long uid, String name, String phoneNumber, String emailId, String password, String address) {
		super();
		this.uid = uid;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.emailId = emailId;
		this.password = password;
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	} 
	public List<Order> getOrders() {
		return orders;
	}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}

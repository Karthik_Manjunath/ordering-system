package com.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.order.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>  {
	User findUserByUid(long id);
	User findUserByName(String name);
}

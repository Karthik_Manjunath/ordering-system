package com.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.order.domain.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
	Item findItemByItemId(long id);
}

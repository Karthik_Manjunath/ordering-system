package com.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.order.domain.ItemsOrdered;

public interface ItemsOrderedRepository extends JpaRepository<ItemsOrdered, Long> {
	ItemsOrdered findItemsOrderedByItemsOrderedId(long id);
}
